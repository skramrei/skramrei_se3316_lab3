<?php
//
// Created by stefa_000 on 11/3/2015 10:55
//

$data = array();
$column = array();

// Read file and retrieve artist data
$artists = fopen("Resources/data-files/artists.txt", "r");
while ($line = fgets($artists)) {
    $column[] = explode("~", $line);
    if (count($column) == 6) {
        $data[] = $column;
        $column = array();
    }
}
fclose($artists);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet" type="text/css">
        <!-- Bootstrap -->
        <link href="Resources/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <!-- CSS Files -->
        <link href="css/artists.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <!-- Top Header -->
            <div id="top-header">
                <div class="container">
                    <nav class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-text">
                                Welcome to <strong>Art Store</strong>,
                                <a href="#" class="navbar-link">Login</a>
                                or
                                <a href="#" class="navbar-link">Create new account</a>
                            </p>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping
                                        Cart</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Logo -->
            <div id="logo-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Art Store</h1>
                        </div>
                        <div class="col-md-4">
                            <form class="form-inline" role="search">
                                <div class="input-group">
                                    <label class="sr-only" for="search">Search</label>
                                    <input id="search" type="text" class="form-control" placeholder="Search" name="search">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Navbar -->
            <div id="navbar">
                <div class="container">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex2-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex2-collapse">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="about.php">About Us</a>
                                </li>
                                <li>
                                    <a href="work.php">Art Works</a>
                                </li>
                                <li class="active">
                                    <a href="artists.php">Artists</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#">Special 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Special 2</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Body -->
            <div class="container">
                <h2>This Week's Best Artists</h2>
                <div class="alert alert-warning" role="alert">Each week we show you who are our best artists ...</div>
                <div class="row">
                    <div id="img-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php for ($i = 0; $i < intval(count($data) / 2); $i++): ?>
                                <div class="item <?php if ($i == 0) { echo "active"; } ?>">
                                    <div class="container">
                                        <?php foreach ($data[$i] as $artist): ?>
                                            <div class="col-md-2">
                                                <div class="thumbnail">
                                                    <img class="img-thumb" src="Resources/art-images/artists/medium/<?php echo $artist[0]; ?>.jpg" alt="<?php echo $artist[1] . " " . $artist[2]; ?>" title="<?php echo $artist[1] . " " . $artist[2]; ?>">
                                                    <br>
                                                    <div class="caption">
                                                        <h4><?php echo $artist[1] . " " . $artist[2]; ?></h4>
                                                        <p><a class="btn btn-info" role="button" href="<?php echo $artist[7] ?>">Learn more</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <a class="left carousel-control" href="#img-carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#img-carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div id="img-carousel2" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php for ($i = intval(count($data) / 2); $i < count($data); $i++): ?>
                                <div class="item <?php if ($i == intval(count($data) / 2)) { echo "active"; } ?>">
                                    <div class="container">
                                        <?php foreach ($data[$i] as $artist): ?>
                                            <div class="col-md-2">
                                                <div class="thumbnail">
                                                    <img class="img-thumb" src="Resources/art-images/artists/medium/<?php echo $artist[0]; ?>.jpg" alt="<?php echo $artist[1] . " " . $artist[2]; ?>" title="<?php echo $artist[1] . " " . $artist[2]; ?>">
                                                    <br>
                                                    <div class="caption">
                                                        <h4><?php echo $artist[1] . " " . $artist[2]; ?></h4>
                                                        <p><a class="btn btn-info" role="button" href="<?php echo $artist[7] ?>">Learn more</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <a class="left carousel-control" href="#img-carousel2" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#img-carousel2" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
                <h4>Artists by Genre</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-info" style="width: 7%">
                        <span>Gothic</span>
                    </div>
                    <div class="progress-bar progress-bar-success" style="width: 27%">
                        <span>Renaissance</span>
                    </div>
                    <div class="progress-bar progress-bar-warning" style="width: 15%">
                        <span>Baroque</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 21%">
                        <span>Pre-Modern</span>
                    </div>
                    <div class="progress-bar" style="width: 30%">
                        <span>Modern</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
