<?php
//
// Created by stefa_000 on 11/3/2015 10:55
//

$data = array();

// Read file and retrieve painting data
$paintings = fopen("Resources/data-files/paintings.txt", "r");
while ($line = fgets($paintings)) {
    switch (explode("~", $line)[3]) {
        case "01330":
        case "01080":
        case "01150":
        case "01180":
        case "01190":
        case "01220":
        case "01140":
        case "01260":
        case "01280":
        case "01290":
        case "01070":
            $data[] = explode("~", $line);
    }
}
fclose($paintings);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316</title>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet" type="text/css">
        <!-- Bootstrap -->
        <link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- CSS files -->
        <link href="css/index.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <!-- NAVBAR -->
            <div id="navbar">
                <div class="container">
                    <div class="navbar navbar-static-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapse" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">LAB 3</a>
                            </div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="index.php">Home</a></li>
                                    <li><a href="about.php">About</a></li>
                                    <li><a href="work.php">Work</a></li>
                                    <li><a href="artists.php">Artists</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CAROUSEL -->
            <div id="imageCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#imageCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#imageCarousel" data-slide-to="1"></li>
                    <li data-target="#imageCarousel" data-slide-to="2"></li>
                    <li data-target="#imageCarousel" data-slide-to="3"></li>
                    <li data-target="#imageCarousel" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <?php for ($i = 0; $i < 5; $i++): ?>
                        <div class="item <?php if ($i == 0) {
                            echo "active";
                        } ?>">
                            <img src="Resources/art-images/paintings/medium/<?php echo $data[$i][3]; ?>.jpg" alt="<?php echo $data[$i][4]; ?>" title="<?php echo $data[$i][4]; ?>">

                            <div class="carousel-caption">
                                <h1><?php echo $data[$i][4]; ?></h1>

                                <p><?php echo $data[$i][6]; ?></p>

                                <p>
                                    <a class="btn btn-lg btn-primary" href="<?php echo $data[$i][12]; ?>" role="button">Learn
                                        More</a>
                                </p>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <a class="left carousel-control" href="#imageCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#imageCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- PAINTINGS -->
            <div class="container">
                <div class="row">
                    <?php for ($i = 5; $i < 8; $i++): ?>
                        <div class="col-lg-4">
                            <img class="img-circle" src="Resources/art-images/paintings/medium/<?php echo $data[$i][3]; ?>.jpg" alt="<?php echo $data[$i][4]; ?>" title="<?php echo $data[$i][4]; ?>">
                            <h2><?php echo $data[$i][4]; ?></h2>
                            <p class="text-justify"><?php echo explode(".", $data[$i][5])[0] . "."; ?></p>
                            <p>
                                <a class="btn btn-default" href="<?php echo $data[$i][12]; ?>" role="button">View details &raquo;</a>
                            </p>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="row">
                    <?php for ($i = 8; $i < 11; $i++): ?>
                        <div class="col-lg-4">
                            <img class="img-circle" src="Resources/art-images/paintings/medium/<?php echo $data[$i][3]; ?>.jpg" alt="<?php echo $data[$i][4]; ?>" title="<?php echo $data[$i][4]; ?>">
                            <h2><?php echo $data[$i][4]; ?></h2>
                            <p class="text-justify"><?php echo explode(".", $data[$i][5])[0] . "."; ?></p>
                            <p>
                                <a class="btn btn-default" href="<?php echo $data[$i][12]; ?>" role="button">View details &raquo;</a>
                            </p>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
