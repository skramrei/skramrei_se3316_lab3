<?php
//
// Created by stefa_000 on 11/3/2015 10:55
//

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet" type="text/css">
        <!-- Bootstrap -->
        <link href="Resources/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <!-- CSS Files -->
        <link href="css/about.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <!-- Top Header -->
            <div id="top-header">
                <div class="container">
                    <nav class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-text">
                                Welcome to <strong>Art Store</strong>,
                                <a href="#" class="navbar-link">Login</a>
                                or
                                <a href="#" class="navbar-link">Create new account</a>
                            </p>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping
                                        Cart</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Logo -->
            <div id="logo-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Art Store</h1>
                        </div>
                        <div class="col-md-4">
                            <form class="form-inline" role="search">
                                <div class="input-group">
                                    <label class="sr-only" for="search">Search</label>
                                    <input id="search" type="text" class="form-control" placeholder="Search" name="search">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Navbar -->
            <div id="navbar">
                <div class="container">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex2-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex2-collapse">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li class="active">
                                    <a href="about.php">About Us</a>
                                </li>
                                <li>
                                    <a href="work.php">Art Works</a>
                                </li>
                                <li>
                                    <a href="artists.php">Artists</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#">Special 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Special 2</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Body -->
            <div class="container">
                <div class="jumbotron">
                    <h2 class="t-shadow">About Us</h2>
                    <p class="t-shadow">
                        This assignment was completed by:<br><br>
                        <em>Stefan Kramreither (skramrei@uwo.ca)</em>
                    </p><br>
                    <p class="t-shadow">It was completed for SE 3316A at Western University</p>
                    <p>
                        <a class="btn btn-lg btn-primary" href="http://owl.uwo.ca/portal" role="button">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
