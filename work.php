<?php
//
// Created by stefa_000 on 11/3/2015 10:55
//

$data = array();

// Read file and retrieve painting data
$paintings = fopen("Resources/data-files/paintings.txt", "r");
while ($line = fgets($paintings)) {
    $data[] = explode("~", $line);
}
fclose($paintings);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet" type="text/css">
        <!-- Bootstrap -->
        <link href="Resources/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <!-- CSS Files -->
        <link href="css/work.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <!-- Top Header -->
            <div id="top-header">
                <div class="container">
                    <nav class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-text">
                                Welcome to <strong>Art Store</strong>,
                                <a href="#" class="navbar-link">Login</a>
                                or
                                <a href="#" class="navbar-link">Create new account</a>
                            </p>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping
                                        Cart</a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Logo -->
            <div id="logo-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Art Store</h1>
                        </div>
                        <div class="col-md-4">
                            <form class="form-inline" role="search">
                                <div class="input-group">
                                    <label class="sr-only" for="search">Search</label>
                                    <input id="search" type="text" class="form-control" placeholder="Search" name="search">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Navbar -->
            <div id="navbar">
                <div class="container">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex2-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex2-collapse">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="about.php">About Us</a>
                                </li>
                                <li class="active">
                                    <a href="work.php">Art Works</a>
                                </li>
                                <li>
                                    <a href="artists.php">Artists</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#">Special 1</a>
                                        </li>
                                        <li>
                                            <a href="#">Special 2</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Body -->
            <div class="container">
                <div class="row">
                    <div id="img-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php $i = true; ?>
                            <?php foreach ($data as $d): ?>
                                <div class="item <?php if ($i == true) {
                                    echo "active";
                                } ?>">
                                    <div class="container">
                                        <div class="col-md-8">
                                            <h2><?php echo $d[4]; ?></h2>
                                            <p><a href="#"><?php echo $d[6]; ?></a></p>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <img class="img-thumbnail img-responsive" src="Resources/art-images/paintings/medium/<?php echo $d[3]; ?>.jpg" alt="<?php echo $d[4]; ?>" title="<?php echo $d[4]; ?>">
                                                </div>
                                                <div class="col-md-7">
                                                    <p class="img-text"><?php echo str_replace("</em>", "", str_replace("<em>", "", $d[5])); ?></p>
                                                    <p class="price"><?php echo $d[11]; ?></p>
                                                    <div class="btn-group btn-group-lg">
                                                        <button class="btn btn-default" type="button">
                                                            <a href="#">
                                                                <span class="glyphicon glyphicon-gift"></span>
                                                                 Add to Wish List
                                                            </a>
                                                        </button>
                                                        <button class="btn btn-default" type="button">
                                                            <a href="#">
                                                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                                                 Add to Shopping Cart
                                                            </a>
                                                        </button>
                                                    </div>
                                                    <p>&nbsp;</p>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Product Details</div>
                                                        <table class="table">
                                                            <tr>
                                                                <th>Date:</th>
                                                                <td><?php echo $d[6]; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Medium:</th>
                                                                <td><?php echo $d[9]; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Dimensions:</th>
                                                                <td><?php echo $d[7] . "cm x " . $d[8] . "cm"; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Home:</th>
                                                                <td><a href="#"><?php echo $d[10]; ?></a></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Link:</th>
                                                                <td><a href="<?php echo $d[12]; ?>">Wiki</a></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i = false; ?>
                            <?php endforeach; ?>
                        </div>
                        <a class="left carousel-control" href="#img-carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#img-carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
